﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Windows.Threading;

namespace BigFixCM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static System.Windows.Forms.Timer LogWatcherTimer = new System.Windows.Forms.Timer();
        static System.Windows.Forms.Timer DisplayTimer = new System.Windows.Forms.Timer();
        Watcher watcher;
        

        string besPath = "";

        public MainWindow()
        {
            InitializeComponent();

            //read in log path


            ModifyRegistry myRegistry = new ModifyRegistry();
            myRegistry.BaseRegistryKey = Microsoft.Win32.Registry.LocalMachine;
            myRegistry.ShowError = true;

            //RESTORE OPTIONS
            myRegistry.SubKeyPath = @"SOFTWARE\Wow6432Node\BigFix\EnterpriseClient";
            if (myRegistry.SubKey == null) myRegistry.SubKeyPath = @"SOFTWARE\BigFix\EnterpriseClient";
            besPath = myRegistry.SubKey.GetValue("EnterpriseClientFolder").ToString();


            ckStartingActions.IsChecked = Properties.Settings.Default.StartingActions;
            ckEndingActions.IsChecked = Properties.Settings.Default.EndingActions;
            ckRelevant.IsChecked = Properties.Settings.Default.RelevantContent;
            ckNOTRelevant.IsChecked = Properties.Settings.Default.NotRelevantContent;
            ckDownloadPing.IsChecked = Properties.Settings.Default.DownloadPings;
            ckGatherAction.IsChecked = Properties.Settings.Default.GatherActions;
            ckReportPosted.IsChecked = Properties.Settings.Default.ReportPosted;
            ckFailedSync.IsChecked = Properties.Settings.Default.FailedtoSync;
            ckSuccessfulSync.IsChecked = Properties.Settings.Default.SuccessfulSync;
            ckStartingClient.IsChecked = Properties.Settings.Default.StartingClient;
            ckClientShutdown.IsChecked = Properties.Settings.Default.ClientShutdown;
            ckRegisterOnce.IsChecked = Properties.Settings.Default.RegisterOnce;
            ckPolling.IsChecked = Properties.Settings.Default.Polling;
            ckRelaySelect.IsChecked = Properties.Settings.Default.RelaySelect;


            /* Adds the event and the event handler for the method that will 
               process the timer event to the timer. */
            LogWatcherTimer.Tick += new EventHandler(LogWatcherTimer_Tick);
            LogWatcherTimer.Interval = 30000; // Properties.Settings.Default.Interval;
            LogWatcherTimer.Start();

            DisplayTimer.Tick += new EventHandler(DisplayTimer_Tick);
            DisplayTimer.Interval = 500; // Properties.Settings.Default.Interval;
            DisplayTimer.Start();


            ////to see what happens, we'll reset the lastTimeStamp to midnight today so we can see all the popups for today
            //TimeSpan ts = new TimeSpan(10, 31, 54);
            //lastTimestamp = lastTimestamp.Date + ts;

            

            //Instead of a timer, we'll just put a hook on the logs folder...
            //this way whenever the log changes, we'll jump into action
            watcher = new Watcher(besPath + @"__BESData\__Global\Logs\");
            watcher.OnChange += new Watcher.OnChangeHandler(OnChanged);
            watcher.StartWatching();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }


        FifoBuffer<QueueItem> queue = new FifoBuffer<QueueItem>(1000);
        bool processQueue = true;

        public void OnChanged(object source, FileSystemEventArgs e)
        {
            Debug.WriteLine("Saw Change");
            try
            {
                this.Dispatcher.InvokeOrExecute(() =>
                {
                        Thread.Sleep(15000);
                        ProcessLog();
                });
            }
            catch { }
        }

        private void LogWatcherTimer_Tick(Object myObject, EventArgs myEventArgs)
        {
            LogWatcherTimer.Stop();
            try
            {
                ProcessLog();
            }
            catch { }
            LogWatcherTimer.Start();
        }



        // This is the method to run when the timer is raised. 
        Notification balloon = null;
        DateTime lastTimestamp = DateTime.Now; //start monitoring from now forward in the log, ignoring previous log entries
        private void DisplayTimer_Tick(Object myObject, EventArgs myEventArgs)
        {
            if (!processQueue || queue.Count <= 0) return;
            processQueue = false; //stop processing till this balloon goes away

            DisplayTimer.Stop();

            QueueItem qi = queue[queue.Count()-1];
            queue.Remove(queue[queue.Count() - 1]);
            Debug.WriteLine("REMOVE:  " + qi.Title + "    " + qi.Message + "    " + qi.Icon);

            try
            {
                balloon = new Notification();
                balloon.BalloonText = qi.Title;
                balloon.BalloonMessage = qi.Message;
                balloon.BalloonIcon = qi.Icon;
                balloon.Unloaded += new RoutedEventHandler(balloon_Unloaded);
                MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, Properties.Settings.Default.TimeOut);
            }
            catch { }


            DisplayTimer.Start();
        }

        void balloon_Unloaded(object sender, RoutedEventArgs e)
        {
            processQueue = true;
        }






        bool processing = false;

        /// <summary>
        /// Process today's log file.  We're looking for new entries and popup messages as the end user wants to see them
        /// </summary>
        private void ProcessLog()
        {
            Debug.WriteLine(lastTimestamp.ToString());

            DateTime timestamp = DateTime.Now;
            string logFile = besPath + @"__BESData\__Global\Logs\" + timestamp.ToString("yyyyMMdd") + ".log";
            if (!File.Exists(logFile)) return;  //nothing to do if the log doesn't exist.

            if (processing) return;

            processing = true;
            Debug.WriteLine("Processing Started");



            //read in today's log
            string log = "";
            using (var fileStream = new FileStream(logFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var textReader = new StreamReader(fileStream))
                {
                    log = textReader.ReadToEnd();
                }
            }


            string[] lines = log.Split(Environment.NewLine.ToCharArray());
            string siteName = "";
            string siteUrl = "";
            string actionNum = "";
            string actionText = "";
            string balloonTitle = "";
            string balloonMessage = "";
            string balloonIcon = "/Images/Info.png";
            bool boolProcess = false;
            int lineCount = 0;
            foreach (string line in lines)
            {
                lineCount++;
                if (line.Trim() == "") continue; //skip to next line for processing
                if (lineCount == 1) continue;
                
                if (line.Trim().ToLower().StartsWith("at"))
                {
                    string[] sline = line.Split(new char[] { '-' }, 3);  //At 01:04:13 -0600 - site_name (url)
                    if (!string.IsNullOrEmpty(sline[2].Trim()))
                    {
                        siteName = sline[2].Split('(')[0].Trim();
                        siteUrl = sline[2].Split('(')[1].TrimEnd(new char[] { ')' }).Trim();
                    }
                    else
                    {
                        siteName = "";
                        siteUrl = "";
                    }
                    string[] time = sline[0].Replace("At", "").Trim().Split(':');
                    TimeSpan ts = new TimeSpan(int.Parse(time[0]), int.Parse(time[1]), int.Parse(time[2]));
                    timestamp = timestamp.Date + ts;

                    boolProcess = true;
                }
                else
                {
                    if (boolProcess)
                    {
                        balloonTitle = "";
                        balloonMessage = "";

                        //we only want to process new entries, not entries from before the last time we checked.
                        Debug.WriteLine(timestamp.ToString() + " > " + lastTimestamp.ToString());
                        if (timestamp >= lastTimestamp)
                        {
                            Debug.WriteLine(lineCount + "  " + timestamp.ToString() + " --- " + line);
                            

                            if (line.Trim().ToLower().StartsWith("actionlogmessage"))
                            {
                                //examples:
                                //   ActionLogMessage: (action 18400) Action signature verified
                                //   ActionLogMessage: (action 18400) starting action

                                if (line.Trim().ToLower().Contains("starting action ") && Properties.Settings.Default.StartingActions)
                                {
                                    balloonIcon = "/Images/media_play_green.png";
                                    balloonTitle = "BigFix Client Action Started";
                                    actionNum = line.Split('(')[1].Split(')')[0].Replace("action", "").Trim();

                                    //now that we have the action number, let's go read in the title of this action
                                    actionText = "";
                                    try
                                    {
                                        using (var fileStream = new FileStream(besPath + @"__BESData\actionsite\Action " + actionNum + ".fxf", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                                        {
                                            using (var textReader = new StreamReader(fileStream))
                                            {
                                                actionText = textReader.ReadToEnd();
                                            }
                                        }
                                        string[] actionLines = actionText.Split(Environment.NewLine.ToCharArray());
                                        foreach (string aline in actionLines)
                                        {
                                            if (aline.Trim().StartsWith("Subject: "))
                                            {
                                                balloonMessage = aline.Replace("Subject:", "").Trim();
                                                break; //we have what we were looking for, so stop processing any further
                                            }
                                        }
                                    }
                                    catch { }
                                }
                                else
                                {
                                    if (line.Trim().ToLower().Contains("ending action ") && Properties.Settings.Default.EndingActions)
                                    {
                                        balloonIcon = "/Images/media_stop_red.png";
                                        balloonTitle = "BigFix Client Action Ended";
                                        actionNum = line.Split('(')[1].Split(')')[0].Replace("action", "").Trim();

                                        //now that we have the action number, let's go read in the title of this action
                                        actionText = "";
                                        try
                                        {
                                            using (var fileStream = new FileStream(besPath + @"__BESData\actionsite\Action " + actionNum + ".fxf", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                                            {
                                                using (var textReader = new StreamReader(fileStream))
                                                {
                                                    actionText = textReader.ReadToEnd();
                                                }
                                            }
                                            string[] actionLines = actionText.Split(Environment.NewLine.ToCharArray());
                                            foreach (string aline in actionLines)
                                            {
                                                if (aline.Trim().StartsWith("Subject: "))
                                                {
                                                    balloonMessage = aline.Replace("Subject:", "").Trim();
                                                    break; //we have what we were looking for, so stop processing any further
                                                }
                                            }
                                        }
                                        catch { }
                                    }
                                }

                            }
                            else if (line.Trim().ToLower().StartsWith("relevant ") && Properties.Settings.Default.RelevantContent)
                            {
                                try
                                {
                                    //new fixlet is reporting relevant... ex:
                                    //   Relevant - ITCS 300: perform scheduled work (pgpstat, hddpass, encryptcheck) (fixlet:18400)
                                    string[] sline = line.Trim().Replace("(", "").Replace(")", "").Split(' ');
                                    string contentType = sline[sline.Count() - 1].Split(':')[0].Trim();
                                    string contentNum = sline[sline.Count() - 1].Split(':')[1].Trim();

                                    balloonIcon = "/Images/check2.png";
                                    balloonTitle = "BigFix Relevant Content (" + contentType + ")";
                                    balloonMessage = line.Trim().Split(new char[] { '-' }, 2)[1];
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("not relevant ") && Properties.Settings.Default.NotRelevantContent)
                            {
                                try
                                {
                                    //new fixlet is reporting not relevant... ex:
                                    //   Not Relevant - Process Compressed & Split Files WAM Settings (Windows) (fixlet:1656)
                                    string[] sline = line.Trim().Replace("(", "").Replace(")", "").Split(' ');
                                    string contentType = sline[sline.Count() - 1].Split(':')[0].Trim();
                                    string contentNum = sline[sline.Count() - 1].Split(':')[1].Trim();

                                    balloonIcon = "/Images/delete2.png";
                                    balloonTitle = "BigFix Not Relevant Content (" + contentType + ")";
                                    balloonMessage = line.Trim().Split(new char[] { '-' }, 2)[1];
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("downloadping ") && Properties.Settings.Default.DownloadPings)
                            {
                                try
                                {
                                    //new action available, relay is notifying client to initiate a download
                                    balloonIcon = "/Images/ping.png";
                                    balloonTitle = "BigFix Download Ping Received";
                                    balloonMessage = "Client received a notice to come gather new content.";
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("failed to synchronize ") && Properties.Settings.Default.FailedtoSync)
                            {
                                try
                                {
                                    //   FAILED to Synchronize - Tivoli Endpoint Manager could not verify the authenticity of the site content. - class NoAuthorizedSignature (class IntendedSiteDoesNotMatch) - gather url - http://9.17.129.163:52311/cgi-bin/bfenterprise/BESGatherMirror.exe?url=http://ibmbfsrvamer.ibm.com:52311/cgi-bin/bfgather.exe/CustomSite_WAM&Time=26Nov16:22:58&rand=b8e42c04&ManyVersionSha1=73ead1e676802697ec4d7aaa2ecb8a3e78cb8fb6

                                    string[] sline = line.Split(new char[]{'-'}, 2);
                                
                                    balloonIcon = "/Images/replace2.png";
                                    balloonTitle = "BigFix Failed to Synchronize";
                                    balloonMessage = sline[1].Trim();
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("successful synchronization ") && Properties.Settings.Default.SuccessfulSync)
                            {
                                try
                                {
                                    //   FAILED to Synchronize - Tivoli Endpoint Manager could not verify the authenticity of the site content. - class NoAuthorizedSignature (class IntendedSiteDoesNotMatch) - gather url - http://9.17.129.163:52311/cgi-bin/bfenterprise/BESGatherMirror.exe?url=http://ibmbfsrvamer.ibm.com:52311/cgi-bin/bfgather.exe/CustomSite_WAM&Time=26Nov16:22:58&rand=b8e42c04&ManyVersionSha1=73ead1e676802697ec4d7aaa2ecb8a3e78cb8fb6

                                    string[] sline = line.Split(new char[] { '-' }, 2);

                                    balloonIcon = "/Images/refresh.png";
                                    balloonTitle = "BigFix Successful Synchronization";
                                    balloonMessage = sline[0].Trim();
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("successful "))
                            {
                            }
                            else if (line.Trim().ToLower().StartsWith("gather "))
                            {
                            }
                            else if (line.Trim().ToLower().StartsWith("beginning relay select "))
                            {
                            }
                            else if (line.Trim().ToLower().StartsWith("report posted ") && Properties.Settings.Default.ReportPosted)
                            {
                                try
                                {
                                    balloonIcon = "/Images/server_from_client.png";
                                    balloonTitle = "BigFix Client Posted Report";
                                    balloonMessage = "Client just posted a report to the server.";
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("download "))
                            {
                            }
                            else if (line.Trim().ToLower().StartsWith("command "))
                            {
                            }
                            else if (line.Trim().ToLower().StartsWith("gatheractionmv") && Properties.Settings.Default.GatherActions)
                            {
                                try
                                {
                                    balloonIcon = "/Images/server_to_client.png";
                                    balloonTitle = "BigFix Gather Initiated";
                                    balloonMessage = "Client initiated a gather new content action.";
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("fixed "))
                            {
                            }
                            else if (line.Trim().ToLower().StartsWith("beginning relay select "))
                            {
                            }
                            else if (line.Trim().ToLower().StartsWith("client shutdown ") && Properties.Settings.Default.ClientShutdown)
                            {
                                try
                                {
                                    balloonIcon = "/Images/lightbulb.png";
                                    balloonTitle = "BigFix Client Stopped";
                                    balloonMessage = line.Trim();
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("registration server version ") && Properties.Settings.Default.RegisterOnce)
                            {
                                try
                                {
                                    balloonIcon = "/Images/certificate.png";
                                    balloonTitle = "BigFix Client Registration";
                                    balloonMessage = line.Trim();
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("starting client version ") && Properties.Settings.Default.StartingClient)
                            {
                                try
                                {
                                    balloonIcon = "/Images/lightbulb_on.png";
                                    balloonTitle = "BigFix Client Started";
                                    balloonMessage = line.Trim();
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("pollforcommands: ") && Properties.Settings.Default.Polling)
                            {
                                try
                                {
                                    balloonIcon = "/Images/server_document.png";
                                    balloonTitle = "BigFix Polling for Commands";
                                    balloonMessage = line.Replace("PollForCommands:","").Trim();
                                }
                                catch { }
                            }
                            else if (line.Trim().ToLower().StartsWith("beginning relay select") && Properties.Settings.Default.Polling)
                            {
                                try
                                {
                                    balloonIcon = "/Images/server_unknown.png";
                                    balloonTitle = "BigFix Selecting Relays";
                                    balloonMessage = line.Trim();
                                }
                                catch { }
                            }




                            //show a new window
                            if (!string.IsNullOrEmpty(balloonTitle) && !string.IsNullOrEmpty(balloonMessage))
                            {
                                Debug.WriteLine("ADD:  " + balloonTitle + "    " + balloonMessage + "    " + balloonIcon);

                                QueueItem qi = new QueueItem();
                                qi.Title = balloonTitle;
                                qi.Message = balloonMessage;
                                qi.Icon = balloonIcon;
                                queue.Add(qi);
                                
                                
                                
                            }


                            lastTimestamp = timestamp;

                        } //timestamp > lasttimestamp

                    } //boolProcess
                }

            }

            Debug.WriteLine("Processing Stopped");
            lastTimestamp = lastTimestamp.AddSeconds(1); //push timer just slightly so the next processing doesn't display notification over and over again.
            processing = false;
        }







































        bool safeToClose = false;
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.StartingActions = ((ckStartingActions.IsChecked.HasValue) ? ckStartingActions.IsChecked.Value : false);
            Properties.Settings.Default.EndingActions = ((ckEndingActions.IsChecked.HasValue) ? ckEndingActions.IsChecked.Value : false);
            Properties.Settings.Default.RelevantContent = ((ckRelevant.IsChecked.HasValue) ? ckRelevant.IsChecked.Value : false);
            Properties.Settings.Default.NotRelevantContent = ((ckNOTRelevant.IsChecked.HasValue) ? ckNOTRelevant.IsChecked.Value : false);
            Properties.Settings.Default.DownloadPings = ((ckDownloadPing.IsChecked.HasValue) ? ckDownloadPing.IsChecked.Value : false);
            Properties.Settings.Default.GatherActions = ((ckGatherAction.IsChecked.HasValue) ? ckGatherAction.IsChecked.Value : false);
            Properties.Settings.Default.ReportPosted = ((ckReportPosted.IsChecked.HasValue) ? ckReportPosted.IsChecked.Value : false);
            Properties.Settings.Default.FailedtoSync = ((ckFailedSync.IsChecked.HasValue) ? ckFailedSync.IsChecked.Value : false);
            Properties.Settings.Default.SuccessfulSync = ((ckSuccessfulSync.IsChecked.HasValue) ? ckSuccessfulSync.IsChecked.Value : false);
            Properties.Settings.Default.StartingClient = ((ckStartingClient.IsChecked.HasValue) ? ckStartingClient.IsChecked.Value : false);
            Properties.Settings.Default.ClientShutdown = ((ckClientShutdown.IsChecked.HasValue) ? ckClientShutdown.IsChecked.Value : false);
            Properties.Settings.Default.RegisterOnce = ((ckRegisterOnce.IsChecked.HasValue) ? ckRegisterOnce.IsChecked.Value : false);
            Properties.Settings.Default.Polling = ((ckPolling.IsChecked.HasValue) ? ckPolling.IsChecked.Value : false);
            Properties.Settings.Default.RelaySelect = ((ckRelaySelect.IsChecked.HasValue) ? ckRelaySelect.IsChecked.Value : false);
            Properties.Settings.Default.Save();


            if (safeToClose)
            {
                //clean up notifyicon (would otherwise stay open until application finishes)
                MyNotifyIcon.Dispose();

                base.OnClosing(e);
            }
            else
            {
                this.Hide();
                e.Cancel = true;
            }
        }



        private void Options_Click(object sender, RoutedEventArgs e)
        {
            this.Show();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            safeToClose = true;
            this.Close();
        }

        private void LearnMore_Click(object sender, RoutedEventArgs e)
        {
            string targetURL = @"http://bigfix.me/clientmonitor"; 
            System.Diagnostics.Process.Start(targetURL);
        }

       

        private void Options_TrayMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            this.Show(); //show this dialog to allow user to change options
        }

    }
}
