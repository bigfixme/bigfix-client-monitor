﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BigFixCM
{
    class QueueItem
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string Icon { get; set; }

        public QueueItem()
        {
            Title = "";
            Message = "";
            Icon = "";
        }
    }
}
