﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Permissions;
using System.Diagnostics;
using System.Windows.Threading;


//http://msdn.microsoft.com/en-us/library/system.io.filesystemwatcher.aspx

namespace BigFixCM
{
    
    public class Watcher
    {
        
        public string directory { get { return watcher.Path; } set { watcher.Path = value; } }

        FileSystemWatcher watcher;

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public Watcher(string localDir)
        {

            // Create a new FileSystemWatcher and set its properties.
            watcher = new FileSystemWatcher();
            watcher.Path = localDir;
            /* Watch for changes in LastAccess and LastWrite times, and
               the renaming of files or directories. */
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            // Only watch text files.
            watcher.Filter = "*";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnWatcherChanged);
            watcher.Created += new FileSystemEventHandler(OnWatcherChanged);
            watcher.Deleted += new FileSystemEventHandler(OnWatcherChanged);
            watcher.Renamed += new RenamedEventHandler(OnWatcherRenamed);

            watcher.IncludeSubdirectories = true;
        }

        public bool watchingNow
        {
            get { return watcher.EnableRaisingEvents; }
        }




        public delegate void OnStartWatchingHandler(object source);
        public event OnStartWatchingHandler OnStartWatching;
        public bool StartWatching()
        {
            try
            {
                watcher.EnableRaisingEvents = true;
                if (OnStartWatching != null) OnStartWatching(this);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public delegate void OnStopWatchingHandler(object source);
        public event OnStopWatchingHandler OnStopWatching;
        public bool StopWatching()
        {
            try
            {
                watcher.EnableRaisingEvents = false;
                if (OnStopWatching != null) OnStopWatching(this);
                return true;
            }
            catch
            {
                return false;
            }
        }



        // Define the event handlers. 
        public delegate void OnChangeHandler(object source, FileSystemEventArgs e);
        public event OnChangeHandler OnChange;
        private void OnWatcherChanged(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            Debug.WriteLine("Saw a Change: " + e.FullPath + " " + e.ChangeType);

            if (OnChange != null) OnChange(source, e);
        }

        public delegate void OnRenamedHandler(object source, RenamedEventArgs e);
        public event OnRenamedHandler OnRename;
        private void OnWatcherRenamed(object source, RenamedEventArgs e)
        {
            // Specify what is done when a file is renamed.
            Debug.WriteLine("Saw a Rename: {0} renamed to {1}", e.OldFullPath, e.FullPath);

            if (OnRename != null) OnRename(source, e);
        }
    }









    public static class DispatcherEx
    {
        public static void InvokeOrExecute(this Dispatcher dispatcher, Action action)
        {
            if (dispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                       action);
            }
        }
    }
}
